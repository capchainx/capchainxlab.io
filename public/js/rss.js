jQuery(function($) {
    //feed to parse
    //www.CapchainX.com - 819a4e83dfeae74e2432cbb46a87f30ed394682d94813e6c448319974c72c27c
    feednami.setPublicApiKey('d88c14fbdd4bfc55681b1aca41a3111ecb770b1e5aae05e8f8bd54e952d83c1f');

    feednami.load('https://medium.com/feed/@CapchainX', function(result){
        if(result.error) {
            console.log(result.error);
            return
        }

        var entries = result.feed.entries;
        var template = '<article><h4><a href="[LINK]" target="_blank">[TITLE]</a></h4><em>[DATE]</em></article>';
        result.feed.entries.forEach(function(entry, i) {
            if(i > 5) {
                return;
            }

            var date = new Date(entry.pubDate);

            var monthNames = [
                'January', 'February', 'March',
                'April', 'May', 'June', 'July',
                'August', 'September', 'October',
                'November', 'December'
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            date = monthNames[monthIndex] + ' ' + day + ', ' + year;

            $('#blog-rss')
                .append(template
                    .replace('[LINK]', entry.link)
                    .replace('[TITLE]', entry.title)
                    .replace('[DATE]', date)
                );
        });
    });

    feednami.load('http://feeds.feedburner.com/Coindesk?format=xml', function(result){
        if(result.error) {
            console.log(result.error);
            return
        }

        var entries = result.feed.entries;
        var template = '<article><h4><a href="[LINK]" target="_blank">[TITLE]</a></h4><em>[DATE]</em></article>';
        result.feed.entries.forEach(function(entry, i) {
            if(i > 5) {
                return;
            }

            var date = new Date(entry.pubDate);

            var monthNames = [
                'January', 'February', 'March',
                'April', 'May', 'June', 'July',
                'August', 'September', 'October',
                'November', 'December'
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            date = monthNames[monthIndex] + ' ' + day + ', ' + year;

            $('#news-rss')
                .prepend(template
                    .replace('[LINK]', entry.link)
                    .replace('[TITLE]', entry.title)
                    .replace('[DATE]', date)
                );
        });
    });
});
