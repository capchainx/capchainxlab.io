jQuery(function($) {
    //feed to parse
    //www.CapchainX.com - 819a4e83dfeae74e2432cbb46a87f30ed394682d94813e6c448319974c72c27c
    feednami.setPublicApiKey('d88c14fbdd4bfc55681b1aca41a3111ecb770b1e5aae05e8f8bd54e952d83c1f');

    feednami.load('https://medium.com/feed/@CapchainX', function(result){
        if(result.error) {
            console.log(result.error);
            return
        }

        var entries = result.feed.entries;
        var template = '<div class="entry clearfix"><div class="entry-image"><a target="_blank" href="[LINK]" data-lightbox="image"><img class="image_fade" src="[IMAGE]" alt="[TITLE]"></a></div><div class="entry-title"><h2><a  target="_blank" href="[LINK]">[TITLE]</a></h2></div><ul class="entry-meta clearfix"><li><i class="icon-calendar3"></i> [DATE]</li></ul><div class="entry-content"><p>[SUMMARY]</p><a target="_blank"  href="[LINK]"class="more-link">Read More</a></div></div>';

        var limit = ($('#blog-rss').attr('data-limit') || 8) - 1;

        var images =  [
            'https://static-wix-blog.wix.com/blog/wp-content/uploads/2015/06/07103833/The-Power-of-An-Effective-FAQ-Page-1.png',
            'http://www.blockchaintechnologies.com/img/blockchain-ledger.jpg',
            'https://www.entercorp.net/images/pic02.jpg',
            'http://www.blockchaintechnologies.com/img/blockchain-consensus.jpg',
            'https://appliedblockchain.com/wp-content/uploads/2016/12/Blockchain-Challenges-Wide.png',
            'https://www.strategy-business.com/media/image/A-Strategists-Guide-to-Blockchain_thumb5_690x400.jpg',
            'https://securecdn.pymnts.com/wp-content/uploads/2017/01/blockchain-data-digest-consortia-fx-patent-intellectual-property-investment.jpg',
            'https://cdn-images-1.medium.com/max/800/1*x3-p4S73k9u5wJe_LN-zOQ.jpeg'
        ];

        result.feed.entries.forEach(function(entry, i) {
            if(i > limit) {
                return;
            }

            var date = new Date(entry.pubDate);

            var monthNames = [
                'January', 'February', 'March',
                'April', 'May', 'June', 'July',
                'August', 'September', 'October',
                'November', 'December'
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            date = monthNames[monthIndex] + ' ' + day + ', ' + year;

            var description = $('<div>'+entry.description+'</div>').text().substr(0, 160) + ' &hellip;';

            $('#blog-rss')
                .prepend(template
                    .replace(/\[LINK\]/ig, entry.link)
                    .replace(/\[TITLE\]/ig, entry.title)
                    .replace(/\[SUMMARY\]/ig, description)
                    .replace(/\[DATE\]/ig, date)
                    .replace(/\[IMAGE\]/ig, images[i])
                );
        });
    });

    feednami.load('http://feeds.feedburner.com/Coindesk?format=xml', function(result){
        if(result.error) {
            console.log(result.error);
            return
        }

        var entries = result.feed.entries;
        var template = '<div class="entry clearfix"><div class="entry-image"><a target="_blank" href="[LINK]" data-lightbox="image"><img class="image_fade" src="[IMAGE]" alt="[TITLE]"></a></div><div class="entry-title"><h2><a  target="_blank" href="[LINK]">[TITLE]</a></h2></div><ul class="entry-meta clearfix"><li><i class="icon-calendar3"></i> [DATE]</li></ul><div class="entry-content"><p>[SUMMARY]</p><a target="_blank"  href="[LINK]"class="more-link">Read More</a></div></div>';

        var limit = ($('#news-rss').attr('data-limit') || 8) - 1;

        var images =  [
            'http://www.blockchaintechnologies.com/img/blockchain-consensus.jpg',
            'https://appliedblockchain.com/wp-content/uploads/2016/12/Blockchain-Challenges-Wide.png',
            'https://www.strategy-business.com/media/image/A-Strategists-Guide-to-Blockchain_thumb5_690x400.jpg',
            'https://img.wonderhowto.com/img/37/83/63583840306353/0/cryptocurrency-for-hackers-part-1-introduction.1280x600.jpg',
            'http://www.blockchaintechnologies.com/img/blockchain-ledger.jpg',
            'https://www.entercorp.net/images/pic02.jpg',
            'http://cdn.pymnts.com/wp-content/uploads/2017/01/blockchain-data-digest-consortia-fx-patent-intellectual-property-investment.jpg',
            'http://lordandladyrealtyblog.co/wp-content/uploads/2017/05/download.png'
        ];

        result.feed.entries.forEach(function(entry, i) {
            if(i > limit) {
                return;
            }

            var date = new Date(entry.pubDate);

            var monthNames = [
                'January', 'February', 'March',
                'April', 'May', 'June', 'July',
                'August', 'September', 'October',
                'November', 'December'
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            date = monthNames[monthIndex] + ' ' + day + ', ' + year;

            var description = $('<div>'+entry.description+'</div>').text().substr(0, 160) + ' &hellip;';

            $('#news-rss')
                .prepend(template
                    .replace(/\[LINK\]/ig, entry.link)
                    .replace(/\[TITLE\]/ig, entry.title)
                    .replace(/\[SUMMARY\]/ig, description)
                    .replace(/\[DATE\]/ig, date)
                    .replace(/\[IMAGE\]/ig, images[i])
                );
        });
    });
});
