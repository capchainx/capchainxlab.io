$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar").addClass("sticky-header");
    } else {
        $(".navbar").removeClass("sticky-header");
    }
});


// on load
Waves.init();

$(".owl-carousel").owlCarousel({
  items:1,
  nav:true,
  loop:true,
  smartSpeed: 1000,
  navText : ["<i class='fas fa-arrow-left'></i>","<i class='fas fa-arrow-right'></i>"]
});

$('.owl-nav button').addClass('waves-effect');

// Custom scrollbar
$(".dv-news").mCustomScrollbar({ 
  theme: 'dark',
 scrollEasing:"linear"
});