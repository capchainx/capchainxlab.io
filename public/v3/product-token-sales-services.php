<?php include('includes/head.php'); ?>
  <?php 
    $currentPage = 'Token';
    include('includes/header.php'); 
  ?>
    <main role="main">
      <div class="dv-banner pt-100" style="background-image: url(img/banner-particles.png);">
        <div class="container">
          <div class="row mt-4">
            <div class="col-12 text-center">
              <div class="dv-title-2 dv-animate">
                <h2>Token Sale Services</h2>
                <span>Token Offering with CapchainX</span>
              </div>
              <div class="sec">
                <h4 class="mb-5 dv-animate">Bespoke ICO services we offer outside the self service platform</h4>
                <div class="clearfix"></div>
                <div class="row">
                  <div class="col-sm-4 dv-animate">
                    <div class="dv-entry">
                      <div class="dv-entry-img">
                        <img src="img/icons/technology.png">
                      </div>
                      <div class="dv-entry-text p-2">
                        <h4>Technology</h4>
                        <ul class="text-left ul-with-icon">
                          <li>ERC 20 / 721 Tokens</li>
                          <li>Auction contracts</li>
                          <li>Vesting and Lock In contracts</li>
                          <li>3rd Party Security Audit report</li>
                          <li>API for ICO status</li>
                          <li>Website mock ups for webpage</li>
                          <li>Technical assistance</li>
                          <li>CapchainX corporate wallet</li>
                          <li>Security with Ledger</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 dv-animate">
                    <div class="dv-entry">
                      <div class="dv-entry-img">
                        <img src="img/icons/business-and-finance.png">
                      </div>
                      <div class="dv-entry-text p-2">
                        <h4>Business And Finance</h4>
                        <ul class="text-left ul-with-icon">
                          <li>Token structuring consultation</li>
                          <li>White paper Assistance</li>
                          <li>Private Investor Network Access</li>
                          <li>Marketing Assistance</li>
                          <li>Exchange Listing Assistance</li>
                          <li>Hedging Assistance</li>
                          <li>Cash out Assistance</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 dv-animate">
                    <div class="dv-entry">
                      <div class="dv-entry-img">
                        <img src="img/icons/legal.png">
                      </div>
                      <div class="dv-entry-text p-2">
                        <h4>Legal</h4>
                        <ul class="text-left ul-with-icon">
                          <li>Legal Assistance (third party law firm)</li>
                          <li>Legal opinion</li>
                          <li>Binding documentation for Crypto Equity</li>
                          <li>Security issuance consultation</li>
                          <li>KYC/AML Assistance with software partner</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="dv-main-content pb-0 pt-0">
        <section class="sec-5 sec sec-pattern">
          <div class="container">
            <h4 class="mb-3 text-center dv-animate">Get in touch with the CapchainX team. We want to know what you think! </h4>
            <div class="row dv-animate">
              <div class="col-sm-8 offset-sm-2">
                <div class="dv-contact-form mt-3">
                  <form>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Name:*</label>
                          <input type="text" class="form-control" id="" aria-describedby="name" placeholder="">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Email:*</label>
                          <input type="email" class="form-control" id="" placeholder="">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Subject:*</label>
                      <input type="text" class="form-control" id="" placeholder="">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Message:*</label>
                      <textarea class="form-control" id="" rows="3" placeholder=""></textarea>
                    </div>
                    <div class="text-right">
                      <button type="submit" class="btn btn-white btn-sm">Contact Us</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="sec-featured sec sec-pattern mt-5">
          <div class="container text-center">
            <h3 class="fancy-title dv-animate">Service Partners</h3>

            <div class="row mt-4">
              <div class="col-md-2 col-sm-4 col-4 mb-3 dv-animate"><img src="img/logos/piper.png" alt="e27"></div>
              <div class="col-md-2 col-sm-4 col-4 mb-3 dv-animate"><img src="img/logos/pwc.png" alt="Forbes"></div>
              <div class="col-md-2 col-sm-4 col-4 mb-3 dv-animate"><img src="img/logos/etherium.png" alt="Tech Crunch"></div>
              <div class="col-md-2 col-sm-4 col-4 mb-3 dv-animate"><img src="img/logos/ether-wallet.png" alt="Crypto Income"></div>
              <div class="col-md-2 col-sm-4 col-4 mb-3 dv-animate"><img src="img/logos/ledger.png" alt="Business Insider"></div>
              <div class="col-md-2 col-sm-4 col-4 mb-3 dv-animate"><img src="img/logos/ethfinex.png" alt="Reuters"></div>
            </div>
          </div>
        </section>
        <?php include('includes/promobox.php'); ?> 
      </div>
    </main>
  <?php include('includes/footer-content.php'); ?>
<?php include('includes/footer.php'); ?>