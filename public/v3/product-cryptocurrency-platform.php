<?php include('includes/head.php'); ?>
  <?php 
    $currentPage = 'Cryptoequity';
    include('includes/header.php'); 
  ?>
    <main role="main">
      <div class="dv-banner pt-100" style="background-position: 0 68%;">
        <div class="container pt-100">
          <div class="row">
            <div class="col-md-6 media">
              <div class="align-self-center">
                <div class="dv-title-2 dv-animate">
                  <h2>CryptoEquity  Platform</h2>
                  <span>Your self-service token sale platform to create tokens and set up auction contracts</span>
                </div>
                <button type="button" class="btn btn-red mt-4 dv-animate">Start your Token Sale</button>
              </div>
            </div>
            <div class="col-md-6 media banner-img-container dv-animate">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/RbePSfxgKJI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </div>
      <div class="dv-main-content sub pb-0">
        <section class="sec-1 sec mt-0 pt-0">
          <div class="container">
            <h4 class="mb-3 text-center dv-animate">Planned Improvements for Future Release</h4>
            <div class="row mt-5">
              <div class="col-md-6 mb-5 mb-md-0 dv-animate">
                 <img src="img/create-tokens.png">
              </div>
              <div class="col-md-6 p-lg-5 dv-animate">
                <h3>Create tokens <span class="c-main">ERC-20</span></h3>
                <p>We specialise in alternative asset-backed tokens. Back your tokens with company equity, debt, patents, a mining claim, land or even your car! </p>
                <p>How? Its not complicated!</p>
                <p>You can properly represent assets with tokens by binding it with contractual documentation. We simply provide you the tools to store binding contracts through the blockchain.</p>
              </div>
            </div>
          </div>
        </section>

        <section class="sec-2 sec sec-pattern pt-0">
          <div class="container">
            <div class="row">
              <div class="col-md-6 p-lg-5 dv-animate">
                <h3 class="mb-0">Setup smart contracts</h3>
                <p class="c-main text-uppercase"><small>Auction, Distribution, Other</small></p>
                <p>You can set up your presale and sale auction contracts by selecting the price behavior of your choosing. We provide you tools to guide you in personalising your auction.</p>
                <p>You can also mange the distribution of your tokens by setting up vesting, lock-in, dividend allocation smart contracts.</p>
                <p>Think of it as your Token CapTable on the blockchain!</p>
              </div>
              <div class="col-md-6 mb-5 mb-md-0 dv-animate">
                 <img src="img/smart-contracts.png">
              </div>
            </div>
          </div>
        </section>

        <section class="sec-3 sec sec-pattern mt-0 pt-0 sec-animate">
          <div class="container">
            <div class="row">
              <div class="col-md-6 mb-5 mb-md-0 dv-animate">
                 <img src="img/manage-assets.png">
              </div>
              <div class="col-md-6 p-lg-5 dv-animate">
                <h3>Manage Assets <span class="c-main">Web Wallet & Portal</span></h3>
                <p>Send/receive Ether & ERC 20 Tokens.</p>
                <p>A convenient way to view transaction history and track token distribution.</p>
                <p>The wallet is highly secure as it supports market leading hardware security wallets (e.g. Ledger Nano S and Metamask).</p>
              </div>
            </div>
          </div>
        </section>

        <section class="sec-4 sec sec-pattern pt-0">
          <div class="container">
            <div class="row">
              <div class="col-md-6 p-lg-5 dv-animate">
                <h3 class="mb-0">Manage Activities</h3>
                <p class="c-main text-uppercase"><small>Dashboard</small></p>
                <p>Our easy to use dashboard allows issuers a secure way to manage and track all token activities in one place.</p>
                <p>Access you smart contract APIs to track activity.</p>
              </div>
              <div class="col-md-6 mb-5 mb-md-0 dv-animate">
                 <img src="img/manage-activities.png">
              </div>
            </div>
          </div>
        </section>

        <?php include('includes/promobox.php'); ?>

      </div>
    </main>
  <?php include('includes/footer-content.php'); ?>
<?php include('includes/footer.php'); ?>