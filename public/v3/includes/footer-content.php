<footer>
  <div class="container text-center">
    <p class="text-uppercase">Follow CapchainX</p>
    <ul class="dv-social-links">
      <li><a href=""><i class="fab fa-facebook-square"></i></a></li>
      <li><a href=""><i class="fab fa-twitter-square"></i></a></li>
      <li><a href=""><i class="fab fa-linkedin"></i></a></li>
      <li><a href=""><i class="fab fa-medium"></i></a></li>
      <li><a href=""><i class="fab fa-youtube"></i></a></li>
      <li><a href=""><i class="fab fa-telegram-plane"></i></a></li>
    </ul>
    
    <ul class="ul-footer-nav mb-4">
      <li><a href="">Home</a></li>
      <li><a href="">Product &amp; Services</a></li>
      <li><a href="">Syndicates</a></li>
      <li><a href="">Resources</a></li>
      <li><a href="">Team</a></li>
      <li><a href="">Login</a></li>
    </ul>

    <div class="dv-copyrights">
      <p>© 2018 CapchainX Ltd. All Rights Reserved.   <a href="">Terms and Conditions</a>   |   <a href="">Privacy Policy</a></p>
    </div>
  </div>
</footer>