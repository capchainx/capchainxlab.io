<header>
  <!-- Fixed navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <a class="navbar-brand" href="../index.php"><img src="img/logo.svg"></a>
    <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-bars"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item <?php if($currentPage =='Home'){echo 'active';}?>">
          <a class="nav-link" href="../index.php">Home</a>
        </li>
        <li class="nav-item <?php if($currentPage =='Product'){echo 'active';}?>">
          <a class="nav-link" href="../product.php">Product &amp; Services</a>
          <ul>
            <li><a href="../product.php" class="<?php if($currentPage =='Product'){echo 'current';}?>">Overview</a></li>
            <li><a href="../product-cryptocurrency-platform.php" class="<?php if($currentPage =='Cryptoequity'){echo 'current';}?>">Cryptoequity Platform (1.0)</a></li>
            <li><a href="../product-distribution-protocol.php" class="<?php if($currentPage =='Distribution'){echo 'current';}?>">Distribution Protocol</a></li>
            <li><a href="../product-token-sales-services.php" class="<?php if($currentPage =='Token'){echo 'current';}?>">Token Sales Services</a></li>
          </ul>
        </li>
        <li class="nav-item <?php if($currentPage =='Syndicates'){echo 'active';}?>">
          <a class="nav-link" href="#">Syndicates</a>
        </li>
        <li class="nav-item <?php if($currentPage =='Resources'){echo 'active';}?>">
          <a class="nav-link" href="#">Resources</a>
        </li>
        <li class="nav-item <?php if($currentPage =='Team'){echo 'active';}?>">
          <a class="nav-link" href="#">Team</a>
        </li>
        <li class="nav-item special">
          <a class="nav-link" href="#">Login</a>
        </li>
      </ul>
    </div>
  </nav>
</header>