<?php include('includes/head.php'); ?>
  <?php 
    $currentPage = 'Distribution';
    include('includes/header.php'); 
  ?>
    <main role="main">
      <div class="dv-banner pt-100" style="background-image: url(img/banner-particles.png);">
        <div class="container">
          <div class="row mt-4">
            <div class="col-12 text-center">
              <div class="dv-title-2 dv-animate">
                <h2>Distribution Protocol</h2>
                <span>Captable Token Wallet and Smart Contracts</span>
              </div>
              <div class="sec">
                <h4 class="mb-5 dv-animate">This protocol facilitates the following services</h4>
                <div class="clearfix"></div>
                <div class="row">
                  <div class="col-sm-4 dv-animate">
                    <div class="dv-entry">
                      <div class="dv-entry-img">
                        <img src="img/icons/vesting-distribution.png">
                      </div>
                      <div class="dv-entry-text">
                        <h4 class="mb-2 text-capitalize">Vesting Distribution</h4>
                        <span>for team members</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 dv-animate">
                    <div class="dv-entry">
                      <div class="dv-entry-img">
                        <img src="img/icons/lock-ins.png">
                      </div>
                      <div class="dv-entry-text">
                        <h4 class="mb-2 text-capitalize">Lock-Ins</h4>
                        <span>for investors/advisors</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 dv-animate">
                    <div class="dv-entry">
                      <div class="dv-entry-img">
                        <img src="img/icons/dividend-alloc.png">
                      </div>
                      <div class="dv-entry-text">
                        <h4 class="mb-2 text-capitalize">Dividend allocation</h4>
                        <span>to existing token holders</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="dv-main-content pb-0 pt-0">
        <section class="sec-1 sec mt-0 pt-0">
          <div class="container">
            <div class="row">
              <div class="col-sm-8 offset-sm-2 text-center dv-animate">
                <p>Future plans for CapchainX 2.0 will integrate captable functionalities to the corporate wallets. As of the moment we currently offer these services outside the self service platform. Get in touch with our team for these services.</p>
                <button type="button" class="btn btn-primary mb-3 dv-animate">Contact Us</button>
              </div>
            </div>
          </div>
        </section>
        <?php include('includes/promobox.php'); ?> 
      </div>
    </main>
  <?php include('includes/footer-content.php'); ?>
<?php include('includes/footer.php'); ?>