<?php include('includes/head.php'); ?>
  <?php 
    $currentPage = 'Product';
    include('includes/header.php'); 
  ?>
    <main role="main">
      <div class="dv-banner pt-100" style="background-image: url(img/banner-particles3.png);">
        <div class="container">
          <div class="row mt-4">
            <div class="col-md-8 offset-md-2 text-center">
              <div class="dv-title-2 mb-4 dv-animate">
                <h2>CapchainX 1.0</h2>
                <span>The First Product Release</span>
              </div>
              <img src="img/computer-2.png" class="mb-5 dv-animate">
              <p class="dv-animate">The self service tokenization platform 1.0 is the first product public release by CapchainX. This release offers clients access to our full security audited protocols that facilitate the creation of standardized ERC20 tokens backed by company equity and the set up of their auction pricing contracts. </p>
            </div>
          </div>
        </div>
      </div>
      <div class="dv-main-content pb-0">
        <section class="sec-1 sec mt-0 pt-0">
          <div class="container">
            <div class="row">
              <div class="col-md-6 mb-5 mb-md-0 dv-animate">
                 <img src="img/tablet-dashboard.png">
              </div>
              <div class="col-md-6 p-lg-4 dv-animate">
                <p>All  accounts come with a CapchainX corporate wallet integrated with Ledger and Metamask.</p>
                <p>The wallet includes a dashboard for the company user to:</p>
                <p class="mb-1"><i class="fas fa-thumbs-up c-main mr-2"></i> Send & receive ether and tokens to and from any address.</p>
                <p class="mb-1"><i class="fas fa-search c-main mr-2"></i> Track the progress of the their token sale,</p>
                <p class="mb-1"><i class="fas fa-chart-line c-main mr-2"></i> View transaction history</p>
              </div>
            </div>
          </div>
        </section>

        <section class="sec-2 sec sec-pattern">
          <div class="container">
            <div class="row">
              <div class="col-md-6 p-lg-4 dv-animate">
                <p>To use CapchainX 1.0, users need to have the following ready: </p>
                <p class="mb-1"><i class="fas fa-check c-green"></i></i> Ledger or Metamask to access our automated services</p>
                <p class="mb-1"><i class="fas fa-check c-green"></i></i> A valid legal opinion that states</p>
                <p class="mb-1 pl-5">(a) The company's authority to issue tokens</p>
                <p class="mb-1 pl-5">(b) The issuance is a utility or a security</p>
                <p class="mb-1 pl-5">(c) If it is a security offering, the company should state that it will follow security laws in order to activate the tokens/auction contracts.</p>
              </div>
              <div class="col-md-6 mb-5 mb-md-0 dv-animate">
                 <img src="img/metamask.png">
              </div>
            </div>
          </div>
        </section>

        <section class="sec-3 sec sec-pattern mt-5 mb-5 dv-future-release">
          <div class="container text-center">
            <h4 class="mb-3 dv-animate">Planned Improvements for Future Release</h4>
            <p class="dv-animate">For future releases of our Self Service tokenization platform 1.0, the following will be considered:</p>

            <div class="row mt-5">
              <div class="col-sm-6 dv-animate-left">
                <div class="dv-entry">
                  <div class="dv-entry-img">
                    <img src="img/icons/multi-sig.png">
                  </div>
                  <div class="dv-entry-text">
                    <h4 class="mb-2">Multi Sig functionality</h4>
                    <p>To allow multiple user permission to access tokens.</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 dv-animate-right">
                <div class="dv-entry">
                  <div class="dv-entry-img">
                    <img src="img/icons/support.png">
                  </div>
                  <div class="dv-entry-text">
                    <h4 class="mb-2">Support for the other assets</h4>
                    <p>Ability to create other asset backed tokens besides Crypto Equity</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="sec-4 sec sec-pattern mt-4 dv-roadmap-section">
          <div class="container text-center">
            <h4 class="mb-3 dv-animate">Roadmap</h4>
            <p class="dv-animate">Ideas that we consider to include on the platform</p>

            <ul class="roadmap">
              <li>Official launch of The Self-service platform <div><span></span></div></li>
              <li>Corporate Wallets <div> <span></span> </div></li>
              <li>Captable capabilities <div> <span></span> </div></li>
              <li>Crypto Equity Exchange <div> <span></span> </div></li>
            </ul>
          </div>
        </section>

        <section class="sec-5 sec sec-pattern mt-4">
          <div class="container">
            <h4 class="mb-3 text-center dv-animate">Get in touch with the CapchainX team. We want to know what you think! </h4>
            
            <div class="row dv-animate">
              <div class="col-sm-8 offset-sm-2">
                <div class="dv-contact-form mt-3">
                  <form>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Name:*</label>
                          <input type="text" class="form-control" id="" aria-describedby="name" placeholder="">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Email:*</label>
                          <input type="email" class="form-control" id="" placeholder="">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Subject:*</label>
                      <input type="text" class="form-control" id="" placeholder="">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Message:*</label>
                      <textarea class="form-control" id="" rows="3" placeholder=""></textarea>
                    </div>
                    <div class="text-right">
                      <button type="submit" class="btn btn-white btn-sm">Contact Us</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
        <?php include('includes/promobox.php'); ?> 
      </div>
    </main>
  <?php include('includes/footer-content.php'); ?>
<?php include('includes/footer.php'); ?>