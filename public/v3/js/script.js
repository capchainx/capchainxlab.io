$(window).scroll(function(){
    if ($(window).scrollTop() > 0) {
      $('header nav').addClass('sticky');
    }
    else {
      $('header nav').removeClass('sticky');
    }
});

const reveal = {
  origin : 'bottom',
  delay    : 200,
  distance : '150px',
  easing   : 'ease-in-out',
  scale : 1
};

window.sr = ScrollReveal(reveal);
sr.reveal('.dv-banner .dv-animate', { duration: 1000}, 200);
sr.reveal('.sec-featured .dv-animate', { duration: 1000, delay: 500}, 200);
sr.reveal('.sec-1 .dv-animate', { duration: 1000, delay: 500}, 200);
sr.reveal('.sec-2 .dv-animate', { duration: 1000, delay: 500}, 200);
sr.reveal('.dv-3-icons .fancy-title', { duration: 1000}, 200);
sr.reveal('.dv-3-icons .dv-animate', { duration: 1000, delay: 800});
sr.reveal('.dv-3-icons .dv-animate-left', { duration: 1000, delay: 800, origin : 'left'});
sr.reveal('.dv-3-icons .dv-animate-right', { duration: 1000, delay: 800, origin : 'right'});
sr.reveal('.sec-4 .dv-animate', { duration: 1000, delay: 500}, 200);
sr.reveal('.dv-promobox .dv-animate', { duration: 1000, delay: 500}, 200);
sr.reveal('.dv-future-release .dv-animate', { duration: 1000}, 200);
sr.reveal('.dv-future-release .dv-animate-left', { duration: 1000, delay: 800, origin : 'left'}, 200);
sr.reveal('.dv-future-release .dv-animate-right', { duration: 1000, delay: 800, origin : 'right'}, 200);

sr.reveal('.roadmap', { duration: 1000, delay: 1100});
sr.reveal('.roadmap li', { duration: 1000, delay: 2000}, 500);

sr.reveal('.sec-5  .dv-animate', { duration: 1000}, 200);
sr.reveal('.sec-animate .dv-animate', { duration: 1000}, 200);
sr.reveal('.ul-with-icon li', { duration: 800}, 100);