<?php include('includes/head.php'); ?>
  <?php 
    $currentPage = 'Home';
    include('includes/header.php'); 
  ?>
    <main role="main">
      <div class="dv-banner pt-100">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="dv-title dv-animate">
                <h1>The First <span>CryptoEquity</span> <span>Platform</span></h1>
                <p class="subtitle"><span>Create</span>, <span>Manage</span>, AND <span>Transfer</span> <br> Asset tokens on the Ethereum blockchain</p>
              </div>
              <button type="button" class="btn btn-red mr-3 mb-3 dv-animate">Start your ICO</button>
              <button type="button" class="btn btn-primary  mb-3 dv-animate">Product Demo</button>
            </div>
            <div class="col-md-6 media banner-img-container dv-animate">
              <?php include('includes/computer-image.php'); ?>
              <!-- <img src="img/computer.png" class="align-self-center"> -->
            </div>
          </div>
        </div>
        <div class="glow-effect"></div>
      </div>
      <div class="dv-main-content pb-0 pt-0">
        <section class="sec-featured sec pt-0">
          <div class="container text-center">
            <h3 class="fancy-title dv-animate">Featured In</h3>
            <div class="row">
              <div class="col-md-2 col-sm-4 col-4 mb-3 dv-animate"><img src="img/logos/e27.png" alt="e27"></div>
              <div class="col-md-2 col-sm-4 col-4 mb-3 dv-animate"><img src="img/logos/forbes.png" alt="Forbes"></div>
              <div class="col-md-2 col-sm-4 col-4 mb-3 dv-animate"><img src="img/logos/techCrunch.png" alt="Tech Crunch"></div>
              <div class="col-md-2 col-sm-4 col-4 mb-3 dv-animate"><img src="img/logos/cryptoIncome.png" alt="Crypto Income"></div>
              <div class="col-md-2 col-sm-4 col-4 mb-3 dv-animate"><img src="img/logos/businessInsider.png" alt="Business Insider"></div>
              <div class="col-md-2 col-sm-4 col-4 mb-3 dv-animate"><img src="img/logos/reuters.png" alt="Reuters"></div>
            </div>
          </div>
        </section>

        <section class="sec-1 sec mt-0 mt-md-5">
          <div class="container">
            <div class="row">
              <div class="col-md-6 mb-5 mb-md-0 dv-animate">
                <?php include('includes/about-cryptoEquity.php'); ?>
                <!-- <img src="img/about-cryptoEquity.png"> -->
              </div>
              <div class="col-md-6 dv-animate">
                <h4>What is Crypto Equity?</h4>
                <p><strong>Crypto Equity</strong> is a sustainable method of representing ownership rights of a company on the blockchain. Just how fiat currency was once backed by gold, basket of more stable currencies and reserves, Crypto Equity tokens are backed by equity. Shares represent ownership, claim to asset, claim to revenue and participation, compensating investors for the risk in case of a default. You could specify the representation of the token through the platform.</p>
                <button type="button" class="btn btn-red mt-4 dv-animate">Start your ICO</button>
              </div>
            </div>
          </div>
        </section>

        <section class="sec-2 sec sec-pattern mt-5">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <h4 class="dv-animate">Who We Work With?</h4>
                <p class="dv-animate">We work with startups, mature companies, government and even social enterprises. The range of industries include tech and security, bio tech, finance, real estate, and mining firms. We serve clients with operations in south africa, switzerland, uk, singapore, philippiens and the rest of the world.</p>

                <div class="row mt-4">
                  <div class="col-md-6 dv-animate">
                    <h5>What is your story?</h5>
                    <p>We'd love to work with you.</p>
                  </div>
                  <div class="col-md-6 dv-animate">
                    <button type="button" class="btn btn-primary mt-2 mb-3">Contact us</button>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row dv-gallery">
                  <div class="col-sm-4 col-4 p-0 dv-animate">
                    <div class="dv-caption">
                      <p>Agriculture</p>
                    </div>
                    <img src="img/gallery/img-1.png" alt="">
                  </div>
                  <div class="col-sm-4 col-4 p-0 dv-animate">
                    <div class="dv-caption">
                      <p>Mining</p>
                    </div>
                    <img src="img/gallery/img-2.png" alt="">
                  </div>
                  <div class="col-sm-4 col-4 p-0 dv-animate">
                    <div class="dv-caption">
                      <p>Government</p>
                    </div>
                    <img src="img/gallery/img-3.png" alt="">
                  </div>
                  <div class="col-sm-4 col-4 p-0 dv-animate">
                    <div class="dv-caption">
                      <p>Agriculture</p>
                    </div>
                    <img src="img/gallery/img-1.png" alt="">
                  </div>
                  <div class="col-sm-4 col-4 p-0 dv-animate">
                    <div class="dv-caption">
                      <p>Mining</p>
                    </div>
                    <img src="img/gallery/img-2.png" alt="">
                  </div>
                  <div class="col-sm-4 col-4 p-0 dv-animate">
                    <div class="dv-caption">
                      <p>Government</p>
                    </div>
                    <img src="img/gallery/img-3.png" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="sec-3 sec sec-pattern mt-5 mb-5 dv-3-icons">
          <div class="container text-center">
            <h3 class="fancy-title">Why us CaphchainX?</h3>

            <div class="row">
              <div class="col-sm-4 dv-animate-left">
                <div class="dv-entry">
                  <div class="dv-entry-img">
                    <img src="img/icons/efficient-transfer.png">
                  </div>
                  <div class="dv-entry-text">
                    <h4>Efficient Transfer</h4>
                    <p>Transfer asset ownership to employees, co-founders and investors faster. Save on legal and brokerage fees.</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-4 dv-animate">
                <div class="dv-entry">
                  <div class="dv-entry-img">
                    <img src="img/icons/secure-transactions.png">
                  </div>
                  <div class="dv-entry-text">
                    <h4>Secure Transactions</h4>
                    <p>Transfer asset ownership to employees, co-founders and investors faster. Save on legal and brokerage fees.</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-4 dv-animate-right">
                <div class="dv-entry">
                  <div class="dv-entry-img">
                    <img src="img/icons/be-in-control.png">
                  </div>
                  <div class="dv-entry-text">
                    <h4>Be in control</h4>
                    <p>Transfer asset ownership to employees, co-founders and investors faster. Save on legal and brokerage fees.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="sec-4 sec sec-pattern mt-4">
          <div class="container text-center">
            <h3 class="fancy-title dv-animate">Clients Testimonials</h3>

            <div id="carouselExampleIndicators" class="carousel slide dv-animate" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/RbePSfxgKJI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="carousel-item">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/RbePSfxgKJI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="carousel-item">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/RbePSfxgKJI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>

          </div>
        </section>
        <?php include('includes/promobox.php'); ?> 
      </div>
    </main>
  <?php include('includes/footer-content.php'); ?>      
<?php include('includes/footer.php'); ?>    